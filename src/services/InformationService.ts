import * as convert from "xml-js";
import * as fs from "fs";
import * as path from 'path';
import axios from "axios";

export default class informationService {
  private _entry;
  private _formatJson;
  private _publicPath;

  constructor() {
    this._entry = "https://dialnet.unirioja.es/oai/OAIHandler?verb=";
    this._formatJson = {
      compact: true,
      spaces: 2
    };
    this._publicPath = `${path.resolve()}/output/`;
  }
  identify() {
    const verb = "Identify";
    return axios(`${this._entry}${verb}`)
      .then(information => {
        const { data } = information;
        return convert.xml2json(data, this._formatJson);
      })
      .catch(err => console.log(err.message));
  }
  listMetadata() {
    const verb = 'ListMetadataFormats';
    return axios(`${this._entry}${verb}`)
    .then(information => {
      const { data } = information;
      return convert.xml2json(data, this._formatJson);
    })
    .catch(err => console.log(err.message));
  }
  listIdentifiers() {
    const verb = 'ListIdentifiers';
    const prefix = 'oai_dc';
    const from = ''; 
    const to = ''
    return axios(`${this._entry}${verb}&metadataPrefix=${prefix}`)
    .then(information => {
      const { data } = information;
      return convert.xml2json(data, this._formatJson);
    })
    .catch(err => console.log(err.message));
  }
  listRecords() {
    const verb = 'ListRecords';
    const prefix = 'oai_dc';
    const from = ''; 
    const to = ''
    return axios(`${this._entry}${verb}&metadataPrefix=${prefix}`)
    .then(information => {
      const { data } = information;
      return convert.xml2json(data, this._formatJson);
    })
    .catch(err => console.log(err.message));
  } 
  record(identifier: string) {
    const verb = 'GetRecord';
    const prefix = 'oai_dc';
    return axios(`${this._entry}${verb}&metadataPrefix=${prefix}&identifier=${identifier}`)
    .then(information => {
      const { data } = information;
      return convert.xml2json(data, this._formatJson);
    })
    
    

  }
  listSets() {
    const verb = 'ListSets';
    return axios(`${this._entry}${verb}`)
    .then(information => {
      const { data } = information;
      return convert.xml2json(data, this._formatJson);
    })
  }
  createFile(verb, content:any,extension = ".json") {
    fs.appendFileSync(`${this._publicPath}${verb}${extension}`, content);
  }
}
