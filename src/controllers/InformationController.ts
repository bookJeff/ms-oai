import InformationService from "../services/InformationService";

const informationController = {
  getIdentify: () => {
    const informationService = new InformationService();
    informationService.identify().then(datos => {
      informationService.createFile("Identity", datos);
    });
  },
  getListMetadata: () => {
    const informationService = new InformationService();
    informationService.listMetadata().then(datos => {
      informationService.createFile("ListMetaData", datos);
    });
  },
  getListIdentifiers: () => {
    const informationService = new InformationService();
    informationService.listIdentifiers().then(datos => {
      informationService.createFile("ListIdentifiers", datos);
    });
  },
  getListRecords: () => {
    const informationService = new InformationService();
    informationService.listRecords().then(datos => {
      informationService.createFile("ListRecords", datos);
    });
  },
  getRecord: () => {
    const informationService = new InformationService();
    const identifier = "oai:dialnet.unirioja.es:ART0000001357";

    informationService.record(identifier).then(datos => {
      informationService.createFile("Record", datos);
    });
  },
  getListSets: () => {
    const informationService = new InformationService();
    informationService.listSets().then(datos => {
      informationService.createFile("ListSets", datos);
    });
  }
};

export default informationController;
