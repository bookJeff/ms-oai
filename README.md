# Protocolo de metadatos OAI PMH

## OAI _Open Archives Initiative_

El planteamiento esencial de los archivos abiertos es permitir el acceso a los materiales en la web por medio de repositorios que, interoperando unos con otros, permitan el intercambio de metadatos, su almacenamiento y publicación.

## PMH _Protocol for Metadata Harvesting_

El Protocolo OAI para la Recolección de Metadatos (OAI-PMH), define un mecanismo para la recolección de registros que contienen los metadatos de los repositorios. El OAI-PMH ofrece a los Proveedores de Datos una opción técnica sencilla para poner sus metadatos a disposición de servicios basados en los estándares abiertos HTTP y XML.

## Funcionamiento

### EntryPoint

El entry de pruebas que actualemnte se usa es:

```url
https://dialnet.unirioja.es/oai/OAIHandler
```

### Parámetros

Los parámetos que se manejan siempre van por metodo **GET**

- **verb**: Define el nombre del metodo que queremos usar.
- **metadataPrefix**: Define el formato de la metadata entregada por el servicio.
- **identifier**: Identificador del registro a buscar.
- **from**: Fecha inicial de busqueda.
- **to**: Fecha final para busqueda.

### Metodos

- **Identify** : Informacion sobre el provedor de servicio, configuracion, codificacion etc.
- **ListMetadataFormats** : Devuelve una lista de datos biograficos del servidor , devuelve tipo de codificacion configurada dublin core __dc__ o alguna otra.
- **ListIdentifiers** : Devuelve las cabeceras de los registros, siempre va acompañado de parametro **metadataPrefix**.
- **ListRecords** : Devuelve los registros completos _cabecera y cuerpo_ de los registos, siempre va acompañado de parametro **metadataPrefix**.
- **GetRecord** : Devuelve un registo en concreto, se acompana con el parametro **identifier** y **metadataPrefix**.
- **ListSets** : Devuelve un conjunto de registros. Estos conjuntos son creados opcionalmente por el servidor para facilitar una recuperación selectiva de metadatos. Se trata de una clasificación de los contenidos según diferentes criterios como materias, lenguaje, tipología documental, etc

