var path = require('path');

module.exports = {
  entry: path.join(__dirname,'./src/app.ts'),
  resolve: {
    alias: {'pg-native': path.join(__dirname, 'aliases/pg-native.js')},
    extensions: [
      '.js',
      '.jsx',
      '.json',
      '.ts',
      '.tsx'
    ]
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    sourceMapFilename: '[file].map'
  },
  target: 'node',
  module: {
    rules: [
      { test: /\.ts(x?)$/, loader: 'awesome-typescript-loader' }
    ]
  },
  optimization: {
    minimize: false
  },
  devtool: 'source-map',
};
